package com.springBoot.database.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.mysql.cj.x.protobuf.MysqlxCrud.Delete;
import com.springBoot.database.dao.AdminDetails;
import com.springBoot.database.model.Admin;
import com.springBoot.database.model.ID;
import com.springBoot.database.model.LoggedInUser;
import com.springBoot.database.model.Student;

import ch.qos.logback.core.net.SyslogOutputStream;

@Controller
@SessionAttributes({"userId","loggedInUser"})
public class AdminRegistrationController {

	@Autowired
	private AdminDetails adminDetails;

	@RequestMapping(value = "/registeradmin", method = RequestMethod.GET)
	public String getAdminRegister(@ModelAttribute("admin") Admin admin,@ModelAttribute("loggedInUser") LoggedInUser l) {

		admin = new Admin();
		return "registeradmin";
	}

	@RequestMapping(value = "/registeradmin", method = RequestMethod.POST)
	public String addAdmin(@ModelAttribute("admin") @Valid Admin admin,BindingResult res,@ModelAttribute("loggedInUser") LoggedInUser l, ModelMap model) {
		if(res.hasErrors()){
			return "registeradmin";
		}
		boolean result = adminDetails.addAdmin(admin);
		if (result) {
			model.addAttribute("report", " Admin Added Successfully..!!");
			return "report";
		}
		model.addAttribute("msg", "User already exists!!!");
		return "registeradmin";
	}

	@RequestMapping(value = "/admindelete", method = RequestMethod.GET)
	public String getDeleteStudent(@ModelAttribute("id") ID id,@ModelAttribute("loggedInUser") LoggedInUser l) {
		id = new ID();
		return "admindelete";
	}

	@RequestMapping(value = "/admindelete", method = RequestMethod.POST)
	public String deleteStudent(@ModelAttribute("id") @Valid ID id,BindingResult res,@ModelAttribute("loggedInUser") LoggedInUser l,ModelMap model) {
		if(res.hasErrors()){
			return "admindelete";
		}
		boolean result = adminDetails.deleteStudent(id.getStudent_id());
		if (result) {
			model.addAttribute("msg", " Student Deleted Successfully..!!");
			return "admindelete";
		}
		model.addAttribute("msg", "User not exists with the given id!!!");
		return "admindelete";
	}

	@RequestMapping(value = "/adminview", method = RequestMethod.GET)
	public String getViewStudent(@ModelAttribute("id") ID id,@ModelAttribute("loggedInUser") LoggedInUser l) {
		id = new ID();
		return "adminview";
	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public String getViewStudentExcel(@ModelAttribute("id") ID id,@ModelAttribute("loggedInUser") LoggedInUser l) {
		id = new ID();
		return "view";
	}

	@RequestMapping(value = "/view", method = RequestMethod.POST)
	public String viewStudent(@ModelAttribute("id") @Valid ID id,BindingResult res,@ModelAttribute("loggedInUser") LoggedInUser l, ModelMap model) {
		if(res.hasErrors()){
			return "adminview";
		}
		boolean result = adminDetails.check(id.getStudent_id());
		if (result) {
			Student student = adminDetails.viewStudent(id.getStudent_id());
			model.addAttribute("student", student);
			return "view";
		}
		model.addAttribute("msg", "User is not present with an given ID!!!");
		return "adminview";
	}

	@RequestMapping(value = "/adminupdate", method = RequestMethod.GET)
	public String updateStudent(@ModelAttribute("id") ID id,@ModelAttribute("loggedInUser") LoggedInUser l, ModelMap model) {
		id = new ID();
		// model.addAttribute("kya",id.getStudent_id());
		return "adminupdate";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String getupdateStudent(@ModelAttribute("id") @Valid ID id,BindingResult result,@ModelAttribute("loggedInUser") LoggedInUser l,ModelMap model) {
		if(result.hasErrors()){
			return "adminupdate";
		}
		boolean res = adminDetails.check(id.getStudent_id());
		if (res) {
			Student student = adminDetails.viewStudent(id.getStudent_id());
			model.addAttribute("student", student);
			return "update";
		}
		model.addAttribute("msg", "User is not present with an given ID!!!");
		return "adminupdate";
	}
	
	@RequestMapping(value = "/viewafterupdate", method = RequestMethod.POST)
	public String getAfterUpdateStudent(@ModelAttribute("student") Student student,@ModelAttribute("loggedInUser") LoggedInUser l, ModelMap model) {
		boolean res =adminDetails.updateStudent(student);
		if (res) {
			student = adminDetails.viewStudent(student.getId());
			model.addAttribute("student", student);
			return "view";
		}
		model.addAttribute("report", "User is not present with an given ID!!!");
		return "report";
	}
	@RequestMapping(value = "/changePasswordA", method = RequestMethod.GET)
	public String changePassword(@ModelAttribute("userId") String id,@ModelAttribute("loggedInUser") LoggedInUser l,ModelMap model) {
		model.addAttribute("message",id);
		return "changePassword";
	}

	@RequestMapping(value = "/changePasswordA", method = RequestMethod.POST)
	public String postchangePassword(@ModelAttribute("userId") String id, @RequestParam String currPass, String newPass,
			String confPass,@ModelAttribute("loggedInUser") LoggedInUser l, ModelMap model) {
		if (newPass.equals(confPass)) {
			boolean res = adminDetails.changePassword(id, newPass, currPass);
			if (res) {
				model.addAttribute("password","Password is successfully changed!!");

				return "adminhomepage";
			} else {
				model.addAttribute("report", "Sorry try once again!");
				return "report";
			}
		}
		model.addAttribute("message", "New Password and Confirm new Password should be same");
		return "changePassword";
	}
}
