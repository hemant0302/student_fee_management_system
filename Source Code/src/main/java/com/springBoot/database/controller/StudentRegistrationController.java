package com.springBoot.database.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.springBoot.database.dao.AdminDetails;
import com.springBoot.database.dao.StudentDetails;
import com.springBoot.database.model.*;

@Controller
@SessionAttributes({"id","loggedInUser"})
public class StudentRegistrationController {

	@Autowired
	private StudentDetails studentDetails;

	@Autowired
	private AdminDetails adminDetails;

	@RequestMapping(value = "/registerstudent", method = RequestMethod.GET)
	public String getRegistrationStudent(@ModelAttribute("student") Student student,@ModelAttribute("loggedInUser") LoggedInUser l) {
		student = new Student();
		return "registerstudent";
	}

	@RequestMapping(value = "/registerstudent", method = RequestMethod.POST)
	public String addStudent(@ModelAttribute("student") @Valid Student student,BindingResult result,@ModelAttribute("loggedInUser") LoggedInUser l, ModelMap model) {
		if(result.hasErrors()){
			return "registerstudent";
		}
		boolean res = studentDetails.addStudent(student);
		if (res) {
			model.addAttribute("id","Your userId is:"+student.getUser_id());
			model.addAttribute("pass","Your password is:"+student.getPassword());
			return "report";
		}
		model.addAttribute("msg", "StudentId already exists!!!");
		return "registerstudent";
	}

	@RequestMapping(value = "/studentView", method = RequestMethod.GET)
	public String viewStudent(@ModelAttribute("id") int id, @ModelAttribute("loggedInUser") LoggedInUser l,ModelMap model) {
		Student student = adminDetails.viewStudent(id);
		model.addAttribute("student", student);
		return "view";

	}

	@RequestMapping(value = "/studentUpdate", method = RequestMethod.GET)
	public String updateStudent(@ModelAttribute("id") int id,@ModelAttribute("loggedInUser") LoggedInUser l, ModelMap model) {
		Student student = adminDetails.viewStudent(id);
		model.addAttribute("student", student);
		return "studentupdate";
	}

	@RequestMapping(value = "/viewafterupdatestudent", method = RequestMethod.POST)
	public String getupdateStudent(@ModelAttribute("student") Student student,@ModelAttribute("loggedInUser") LoggedInUser l, ModelMap model) {
		boolean res = studentDetails.updateStudent(student);
		student = adminDetails.viewStudent(student.getId());
		model.addAttribute("student", student);
		return "view";
	}

	@RequestMapping(value = "/changePasswordS", method = RequestMethod.GET)
	public String changePassword(@ModelAttribute("id") int id,@ModelAttribute("loggedInUser") LoggedInUser l, ModelMap model) {
		return "changePasswordstudent";
	}

	@RequestMapping(value = "/changePasswordS", method = RequestMethod.POST)
	public String postchangePassword(@ModelAttribute("id") int id, @RequestParam String currPass, String newPass,
			String confPass,@ModelAttribute("loggedInUser") LoggedInUser l, ModelMap model) {
		if (newPass.equals(confPass)) {
			boolean res = studentDetails.changePassword(id, newPass);
			if (res) {
				model.addAttribute("password","Password is successfully changed!!");
				return "studentview";
			}
			else{
				model.addAttribute("report", "Sorry try once again!");
				return "report";
			}
		}
		model.addAttribute("message", "New Password and Confirm new Password should be same");
		return "changePassword";
	}

	@ModelAttribute("radio")
	public List<String> radioOptions() {
		List<String> radio = new ArrayList<>();
		radio.add("Male");
		radio.add("Female");
		radio.add("Others");
		return radio;
	}
}
