package com.springBoot.database.controller;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.springBoot.database.model.LoggedInUser;

@Controller
@SessionAttributes("loggedInUser")
public class LogInLogOutController {
	
	@ModelAttribute("loggedInUser")
	public LoggedInUser loggedInUser(){
		return new LoggedInUser();
	}
	@GetMapping("/header")
	public String showHeader(@ModelAttribute("loggedInUser") LoggedInUser l){
		return "header";
	}
	@GetMapping("/logout")
	public String Logout(@ModelAttribute("loggedInUser") LoggedInUser l,BindingResult res){
		l.setRole(null);
		l.setUserID(null);
		return "redirect:/login";
		
	}
}
