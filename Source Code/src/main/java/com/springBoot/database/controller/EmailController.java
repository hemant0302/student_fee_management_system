package com.springBoot.database.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.springBoot.database.dao.AdminDetails;
import com.springBoot.database.dao.StudentDetails;
import com.springBoot.database.model.Admin;
import com.springBoot.database.model.Mail;
import com.springBoot.database.model.Student;
import com.springBoot.database.service.EmailService;

@org.springframework.stereotype.Controller
public class EmailController {

	@Autowired
	private EmailService emailService;

	@Autowired
	private AdminDetails adminDetails;

	@Autowired
	private StudentDetails studentDetails;

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String getIndex(@ModelAttribute("mail") Mail mail) {
		return "index";
	}

	@RequestMapping(value = "/confirm", method = RequestMethod.POST)
	public String getConfirm(@ModelAttribute("mail") Mail mail, ModelMap map) {

		try {
			if (mail.getBody().equals("Admin")) {
				if (adminDetails.checkAdmin(mail.getTo())) {
					Admin a = adminDetails.forgotPassword(mail);
					emailService.sendMail(a.getEmailId(), a.getPassword());
				} else {
					map.addAttribute("msg", "User is not found");
					return "index";
				}
			} else {
				if (adminDetails.check(Integer.parseInt(mail.getTo()))) {
					Student s = studentDetails.forgotPassword(mail);
					emailService.sendMail(s.getEmailId(), s.getPassword());
				}
				else{
					map.addAttribute("msg", "User is not found");
					return "index";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("message", "Something went wrong!!!!!");
			return "confirm";
		}
		map.put("message", "Mail successfully sent!!!!!");
		return "confirm";
	}

	@ModelAttribute("r")
	public List<String> radioOptions() {
		List<String> r = new ArrayList<>();
		r.add("Admin");
		r.add("Student");
		return r;
	}

}
