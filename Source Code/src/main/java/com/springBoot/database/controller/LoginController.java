package com.springBoot.database.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.springBoot.database.dao.AdminDetails;
import com.springBoot.database.dao.StudentDetails;
import com.springBoot.database.model.LoggedInUser;
import com.springBoot.database.model.Login;

@Controller
@SessionAttributes({ "id", "userId", "loggedInUser" })

public class LoginController {

	@Autowired
	private AdminDetails adminDetails;

	@Autowired
	private StudentDetails studentDetails;

	@ModelAttribute("loggedInUser")
	public LoggedInUser loggedInUser() {
		return new LoggedInUser();
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String showLoginPage(@ModelAttribute("login") Login login) {
		login = new Login();
		return "login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String LoginPage(@ModelAttribute("login") Login login, ModelMap model) {
		model.remove("id");
		model.remove("userId");
		login = new Login();
		return "login";
	}

	@RequestMapping(value = "/homepage", method = RequestMethod.GET)
	public String redirectedHomePage(@ModelAttribute("loggedInUser") LoggedInUser loggedInUser) {
		return "adminhomepage";
	}

	@RequestMapping(value = "/homepageS", method = RequestMethod.GET)
	public String redirectedHomePages(@ModelAttribute("loggedInUser") LoggedInUser loggedInUser) {
		return "studentview";
	}

	@RequestMapping(value = "/homepage", method = RequestMethod.POST)
	public String showHomePage(@ModelAttribute("login") Login login, @ModelAttribute("loggedInUser") LoggedInUser l,
			ModelMap model) {
		l.setRole(login.getRole());
		l.setUserID(login.getUser_id());
		if (login.getRole().equals("Admin")) {

			boolean res = adminDetails.validateAdmin(login.getUser_id(), login.getPassword());
			if (res) {
				String s = login.getUser_id();
				model.put("userId", s);
				return "adminhomepage";
			} else {
				model.addAttribute("msgs", "UserId or Password is incorrect...Please try again!");
				return "login";
			}
		} else {

			boolean res = studentDetails.validateStudent(login.getUser_id(), login.getPassword());
			if (res) {
				int id = studentDetails.getId(login.getUser_id());
				model.put("id", id);
				return "studentview";
			} else {
				model.addAttribute("msgs", "UserId or Password is incorrect...Please try again!");
				return "login";
			}
		}

	}

	@ModelAttribute("r")
	public List<String> radioOptions() {
		List<String> r = new ArrayList<>();
		r.add("Admin");
		r.add("Student");
		return r;
	}
}
