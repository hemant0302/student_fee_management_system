package com.springBoot.database.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.springBoot.database.model.Admin;
import com.springBoot.database.model.Mail;
import com.springBoot.database.model.Student;
import com.springBoot.database.rowMapper.AdminRowMapper;
import com.springBoot.database.rowMapper.StudentRowMapper;

@Service
public class AdminDetails {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	/*
	 * public boolean validateAdmin(Admin admin){ Object
	 * userId=admin.getUser_id(); String password=admin.getPassword(); String
	 * q="select password from admin_details where user_id="+admin.getUser_id()+
	 * ""; List r = jdbcTemplate.queryForList(q); return (p.equals(password)); }
	 */
	public boolean validateAdmin(String id, String password) {
		String q = "select * from admin_details where user_id=?";
		try {
			AdminRowMapper adminRowMapper = new AdminRowMapper();
			Admin a = jdbcTemplate.queryForObject(q, adminRowMapper, id);
			if (a.getPassword().equals(password)) {
				return true;
			}
			return false;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean addAdmin(Admin admin) {
		String q = "insert into admin_details values(?,?,?,?)";
		int x;
		try {
			x = jdbcTemplate.update(q, admin.getName(), admin.getEmailId(), admin.getPassword(), admin.getUser_id());
		} catch (Exception e) {
			return false;
		}
		return x == 1;
	}

	public boolean addStudent(Student student) {

		String st = student.getName() + student.getId();
		student.setPassword(st);
		student.setUser_id(st);
		student.setFees(0);

		String q = "insert into student_details values(?,?,?,?,?,?,?,?,?,?)";
		int x;
		try {
			x = jdbcTemplate.update(q, student.getId(), student.getName(), student.getPassword(), student.getDate(),
					student.getMobile(), student.getAddress(), student.getUser_id(), student.getFees(),
					student.getGender(), student.getEmailId());
		} catch (Exception e) {
			return false;
		}
		return x == 1;
	}

	public boolean deleteStudent(int id) {

		String q = "delete from student_details where id=?";
		int x;
		try {
			x = jdbcTemplate.update(q, id);
		} catch (Exception e) {
			return false;
		}
		return x == 1;
	}

	public boolean updateStudent(Student student) {
		
		String q = "update student_details set fees=? where id=?";
		int x;
		try{
		x = jdbcTemplate.update(q, student.getFees(),student.getId());
		}catch(Exception e){
			return false;
		}
		return x==1;
	}

	/*
	 * public List<Student> viewAllStudent(){ return
	 * jdbcTemplate.query("select * from student_details",new
	 * RowMapper<Student>(){
	 * 
	 * @Override public Student mapRow(ResultSet rs, int rownumber) throws
	 * SQLException { Student student=new Student();
	 * student.setId(rs.getInt(1)); student.setName(rs.getString(2));
	 * student.setPassword(rs.getString(3));
	 * student.setEmailId(rs.getString(4)); student.setDate(rs.getString(5));
	 * student.setMobile(rs.getLong(6)); student.setAddress(rs.getString(7));
	 * student.setFee(rs.getInt(8)); student.setUserId(rs.getString(9)); return
	 * student; } }); }
	 */
	public Admin forgotPassword(Mail mail) {
		String q = "select * from admin_details where user_id=?";
		try {
			AdminRowMapper adminRowMapper = new AdminRowMapper();
			Admin a = jdbcTemplate.queryForObject(q, adminRowMapper, mail.getTo());
			return a;
		} catch (Exception e) {
			return new Admin();
		}
	}
	public boolean check(int id) {
		String q = "select * from student_details where id=?";
		try {
			StudentRowMapper adminRowMapper = new StudentRowMapper();
			Student a = jdbcTemplate.queryForObject(q, adminRowMapper, id);
			if (a.getId() == id) {
				return true;
			}
			return false;
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean checkAdmin(String id) {
		String q = "select * from admin_details where user_id=?";
		try {
			AdminRowMapper adminRowMapper = new AdminRowMapper();
			Admin a = jdbcTemplate.queryForObject(q, adminRowMapper, id);
			if (a.getUser_id().equals(id)) {
				return true;
			}
			return false;
		} catch (Exception e) {
			return false;
		}
	}

	public Student viewStudent(int id) {

		String q = "select * from student_details where id=?";
		try {
			StudentRowMapper studentRowMapper = new StudentRowMapper();
			Student st = jdbcTemplate.queryForObject(q, studentRowMapper, id);
			return st;
		} catch (Exception e) {
			return new Student();
		}
	}
	public boolean changePassword(String userId, String newPass, String currPass) {
		String s = "select * from admin_details where user_id=?";
		int x = 0;
		try {
			AdminRowMapper adminRowMapper = new AdminRowMapper();
			Admin a = jdbcTemplate.queryForObject(s, adminRowMapper, userId);
			if (a.getPassword().equals(currPass)) {
				String q = "update admin_details set password=? where user_id=?";
				x = jdbcTemplate.update(q, newPass, userId);
			}
			else{
				return false;
			}
		} catch (Exception e) {
			return false;
		}
		return x == 1;
	}
}
