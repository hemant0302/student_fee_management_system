package com.springBoot.database.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.springBoot.database.model.Admin;
import com.springBoot.database.model.Mail;
import com.springBoot.database.model.Student;
import com.springBoot.database.rowMapper.AdminRowMapper;
import com.springBoot.database.rowMapper.StudentRowMapper;

@Service
public class StudentDetails {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public int getId(String userId) {

		String q = "select * from student_details where user_id=?";
		try {
			StudentRowMapper studentRowMapper = new StudentRowMapper();
			Student s = jdbcTemplate.queryForObject(q, studentRowMapper, userId);
			return s.getId();
		} catch (Exception e) {
			return 0;
		}
	}

	public boolean check(int id) {
		String q = "select * from student_details where id=?";
		try {
			StudentRowMapper adminRowMapper = new StudentRowMapper();
			Student a = jdbcTemplate.queryForObject(q, adminRowMapper, id);
			if (a.getId() == id) {
				return true;
			}
			return false;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean validateStudent(String id, String password) {
		String q = "select * from student_details where user_id=?";
		try {
			StudentRowMapper studentRowMapper = new StudentRowMapper();
			Student s = jdbcTemplate.queryForObject(q, studentRowMapper, id);
			if (s.getPassword().equals(password)) {
				return true;
			}
			return false;
		} catch (Exception e) {
			return false;
		}
		// return true;
	}

	public boolean addStudent(Student student) {

		String st = student.getName() + student.getId();
		student.setPassword(st);
		student.setUser_id(st);
		student.setFees(0);

		String q = "insert into student_details values(?,?,?,?,?,?,?,?,?,?)";
		int x;
		try {
			x = jdbcTemplate.update(q, student.getId(), student.getName(), student.getPassword(), student.getDate(),
					student.getMobile(), student.getAddress(), student.getUser_id(), student.getFees(),
					student.getGender(), student.getEmailId());
		} catch (Exception e) {
			return false;
		}
		return x == 1;
	}

	public boolean updateStudent(Student student) {
		
		//String st = student.getName() + student.getId();
		//student.setPassword(st);
		//student.setUser_id(st);
		//student.setFees(0);

		String q = "update student_details set name=?,date=?,mobile=?,address=?,gender=?,emailId=? where id=?";
		int x;
		try{
		x = jdbcTemplate.update(q, student.getName(), student.getDate(),student.getMobile(),student.getAddress(),student.getGender(),student.getEmailId(),student.getId());
		}catch(Exception e){
			return false;
		}
		return x == 1;
	}
	public Student forgotPassword(Mail mail) {

		String q = "select * from student_details where id=?";
		try {
			StudentRowMapper studentRowMapper = new StudentRowMapper();
			Student st = jdbcTemplate.queryForObject(q, studentRowMapper, Integer.parseInt(mail.getTo()));
			return st;
		} catch (Exception e) {
			return new Student();
		}
	}

	public boolean changePassword(int id, String newPass) {
		String q = "update student_details set password=? where id=?";
		int x;
		try {
			x = jdbcTemplate.update(q, newPass, id);
		} catch (Exception e) {
			return false;
		}
		return x == 1;
	}
}
