package com.springBoot.database.rowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.springBoot.database.model.Admin;
import com.springBoot.database.model.Student;

public class StudentRowMapper implements RowMapper<Student> {
	public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
		Student a = new Student();
		a.setId(rs.getInt(1));
		a.setName(rs.getString(2));
		a.setPassword(rs.getString(3));
		a.setDate(rs.getString(4));
		a.setMobile(rs.getString(5));
		a.setAddress(rs.getString(6));
		a.setUser_id(rs.getString(7));
		a.setFees(rs.getDouble(8));
		a.setGender(rs.getString(9));
		a.setEmailId(rs.getString(10));
		return a;
	}
}
