package com.springBoot.database.rowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.springBoot.database.model.Admin;

public class AdminRowMapper implements RowMapper<Admin> {
	public Admin mapRow(ResultSet rs, int rowNum) throws SQLException {
		Admin a = new Admin();
		a.setName(rs.getString(1));
		a.setEmailId(rs.getString(2));
		a.setPassword(rs.getString(3));
		a.setUser_id(rs.getString(4));
		return a;
	}
}
