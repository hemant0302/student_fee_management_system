package com.springBoot.database.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

public class ID {
	
	private int student_id;

	public int getStudent_id() {
		return student_id;
	}

	public void setStudent_id(int student_id) {
		this.student_id = student_id;
	}

}
