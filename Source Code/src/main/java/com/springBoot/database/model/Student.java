package com.springBoot.database.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class Student {
	//@Min(value=0,message="Enter a valid id number..")
	private int id;
    @NotEmpty(message="Name shouldn't be empty..")
    private String name;
	private String password;
	@NotEmpty(message="DOB shouldn't be empty..")
	private String date;
	@NotEmpty(message="Mobile Number shouldn't be empty..")
	@Pattern(regexp="^[7-9]{1}[0-9]{9}",message="Enter a valid mobile number")
	private String mobile;
	@NotEmpty(message="Address shouldn't be empty..")
	private String address;
	private String user_id;
	private double fees;
	@NotEmpty(message="Gender shouldn't be empty..")
	private String gender;
	@NotEmpty(message="EmailId shouldn't be empty..")
	@Email
	private String emailId;
	
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id =user_id;
	}
	public double getFees() {
		return fees;
	}
	public void setFees(double fees) {
		this.fees = fees;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
}
