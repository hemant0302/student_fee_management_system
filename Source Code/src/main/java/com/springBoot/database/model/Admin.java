package com.springBoot.database.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class Admin {
	
	@NotEmpty(message="Name shouldn't be empty..")
	private String name;
	
	@NotEmpty(message="Email shouldn't be empty..")
	@Email
	private String emailId;
	
	@Pattern(regexp="^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$",message="Must be alphanumeric,1 captial alphabet and 1 special character..")
	private String password;
	
	@NotEmpty(message="UserId shouldn't be empty..")
	private String user_id;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public Admin(String name, String emailId, String password, String user_id) {
		super();
		this.name = name;
		this.emailId = emailId;
		this.password = password;
		this.user_id = user_id;
	}
	public Admin() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
