<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<title>Login</title>
<!-- <link href="css/login.css" rel="stylesheet"> -->
<!-- <link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css"
	rel="stylesheet"> -->
<style>
#t {
	border-collapse: separate;
	border-spacing: 20px 15px;
}

span {
	display: inline-block;
	width: 600px;
	height: 300px;
	margin: 6px;
	background-color: #e7e7e7;
}

/* .btn {
  color: white;
} */
a {
	color: white;
}

a:hover {
	color: white;
}

body {
	background-image:
		url('../images/login.jpg');
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: 100% 100%;
}

#mydiv {
	position: fixed;
	top: 45%;
	left: 30%;
	transform: translate(-50%, -50%);
}
</style>
</head>
<body>

	<div id="mydiv">
		<div class="container-fluid">
			<span class="border border-success rounded-lg">
				<center>
					<h1 class="page-header text-center" style="color: #2C3E50">Login</h1>
					<form:form method="POST" action="/homepage" modelAttribute="login">


						<div>
							<table id="t">
								<tr>
									<td">${msgs}</td>
								</tr>
								<tr>
									<td><sf:label path="user_id">Enter User Id :</sf:label></td>
									<td><sf:input path="user_id" id="userIdLogin" type="text"
											name="userId" required="required" /></td>
								</tr>
								<tr>
									<td><sf:label path="password">Enter Password :</sf:label></td>
									<td><sf:password path="password" id="passwordLogin"
											name="password" required="required" /></td>
								</tr>
								<tr>
									<td><label>Role:</label></td>
									<td><sf:select path="role" items="${r}" /></td>

								</tr>
							</table>

						</div>

						<table>
							<tr>
								<td><input class="btn btn-success" type="submit"
									value="Login" id="submit" name="submit"></td>
								<td><sf:button type="button" class="btn btn-info">
										<a href="/registerstudent">Register as a Student</a>
									</sf:button></td>
								<td><sf:button type="button" class="btn btn-info">
										<a href="/registeradmin">Register as an Admin</a>
									</sf:button></td>
								<td><sf:button type="button" class="btn btn-warning">
										<a href="/index">Forget Password</a>
									</sf:button></td>
							</tr>
						</table>

					</form:form>
				</center>
			</span>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
		
</body>
</html>