<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Delete</title>
<style type="text/css">
.t {
	border-collapse: separate;
	border-spacing: 70px 30px;
}

body {
	background-image: url('../images/updatestud.jpg');
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: 100% 100%;
}

#mydiv {
	position: fixed;
	top: 45%;
	left: 70%;
	transform: translate(-50%, -50%);
}
</style>
</head>
<body>
	<jsp:include page="header.jsp" />
	<c:if test="${not empty loggedInUser.userID}">
		<form:form method="post" action="/viewafterupdatestudent"
			modelAttribute="student">
			<table class="t" id="mydiv" style="color: #FF0000">
				<tr>
					<td><label>Student ID</label></td>
					<td><form:input path="id" value="${student.id}"
							readonly="true" /></td>
				</tr>
				<tr>
					<td><label>Name</label></td>
					<td><form:input path="name" value="${student.name}" /></td>
				</tr>
				<tr>
					<td><label>Date of Birth</label></td>
					<td><form:input path="date" value="${student.date}" /></td>
				</tr>
				<tr>
					<td><label>Mobile No.</label></td>
					<td><form:input path="mobile" value="${student.mobile}" /></td>
				</tr>
				<tr>
					<td><label>Address</label></td>
					<td><form:input path="address" value="${student.address}" /></td>
				</tr>
				<tr>
					<td><label>User ID</label></td>
					<td><form:input path="user_id" value="${student.user_id}"
							readonly="true" /></td>
				</tr>
				<tr>
					<td><label>Fees</label></td>
					<td><form:input path="fees" value="${student.fees}"
							readonly="true" /></td>
				</tr>
				<tr>
					<td><label>Gender</label></td>
					<td><form:input path="gender" value="${student.gender}" /></td>
				</tr>
				<tr>
					<td><label>Email ID</label></td>
					<td><form:input path="emailId" value="${student.emailId}" /></td>
				</tr>
				<tr>
					<td><input class="btn btn-success" type="submit"
						value="Submit"></td>
					<td><a class="btn btn-primary" href="/homepageS">Back</a>
				</tr>
			</table>
		</form:form>
	</c:if>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
</body>
</html>
