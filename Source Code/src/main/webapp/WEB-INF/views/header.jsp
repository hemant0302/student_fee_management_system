<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head lang="en">

<title>Header</title>
<style>
body {

       max-width: 1600px;
       margin: auto;
}

.header {
       display: flex;
       justify-content: space-between;
       align-items: center;
       height: 50px;
       border-bottom: 1px solid lightgray;
}

.header .right {
       display: flex;
       align-items: center;
}

.header .h6 {
       margin: 4px;
}

.header a {
       color: white;
       background-color: #4CAF50;
       padding: 6px;
       text-decoration: none;
       flex-grow: 0;
       display: block;
       border-radius: 4px;
       margin: 0 4px;
       height: fit-content;
}

</style>
</head>
<body>
	<div class="header">
	<c:if test="${not empty loggedInUser.userID}">
	<div class="right">
		<h2 style="color: yellow">${ loggedInUser.userID}</h2>
		<a href="/logout" class="registerbtn">Logout</a>
	</div>
	</c:if>
	<c:if test="${empty loggedInUser.userID}">
	<div class="right">
		
		<a href="/" class="registerbtn">Login</a>
	</div>
	</c:if>
	</div>

</body>
</html>