<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
body {
	background-image: url('../images/confirm.jpg');
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: 100% 100%;
	color: red;
}

#mydiv {
	position: fixed;
	top: 28%;
	left: 50%;
	transform: translate(-50%, -50%);
}
</style>
</head>
<body>
<jsp:include page="header.jsp" />
	<c:if test="${not empty loggedInUser.userID}">
		<div id="mydiv">
			<h1 align="center">${message}</h1>
		</div>
	</c:if>
</body>
</html>