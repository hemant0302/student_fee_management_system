<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<title>STUDENT REGISTRATION</title>
<!-- <link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css"
	rel="stylesheet"> -->
<style>
#t {
	border-collapse: separate;
	border-spacing: 100px 20px;
	width: 50%;
}

/* span {
	display: inline-block;
	width: 500px;
	height: 600px;
	margin: 6px;
	background-color: #F8F9F9;
} */
td {
	color: #FDFEFE;
	font-weight: bold;
	font-size: 70%;
}

body {
	background-image: url('../images/studentregistration.jpg');
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: 100% 100%;
}

#mydiv {
	position: fixed;
	top: 50%;
	left: 70%;
	transform: translate(-50%, -50%);
}
</style>

</head>
<body>
		<div id="mydiv">
			<div class="container-fluid">
				<span class="border border-success rounded-lg">
					<center>
						<h1 class="page-header text-center" style="color: #3498DB">STUDENT
							REGISTRATION PAGE</h1>

						<div class="container" align="center">
							<form:form method="post" modelAttribute="student">
								<table id="t">
									<tr>
										<td>${msg}</td>
									</tr>
									<%-- <tr>
			<td><form:label path="id">User Id</form:label></td>
			<td><form:input path="id"/></td>
			</tr> --%>
									<tr>
										<td><form:errors path="id" /></td>
										<td><form:label path="id">Student_ID</form:label></td>
										<td><form:input path="id" required="required" /></td>

									</tr>
									<tr>
										<td><form:errors path="name" /></td>
										<td><form:label path="name">Name</form:label></td>
										<td><form:input path="name" /></td>

									</tr>

									<%-- <tr>
			<td><form:label path="password">Password</form:label></td>
			<td><form:input path="password" type="password"/></td>
			</tr> --%>

									<tr>
										<td><form:errors path="date" /></td>
										<td><form:label path="date">Date of Birth</form:label></td>
										<td><form:input path="date" type="date" /></td>

									</tr>

									<tr>
										<td><form:errors path="mobile" /></td>
										<td><form:label path="mobile">Mobile No.</form:label></td>
										<td><form:input path="mobile" /></td>

									</tr>

									<tr>
										<td><form:errors path="address" /></td>
										<td><form:label path="address">Address</form:label></td>
										<td><form:input path="address" /></td>

									</tr>

									<tr>
										<td><form:errors path="emailId" /></td>
										<td><form:label path="emailId">Email ID</form:label></td>
										<td><form:input path="emailId" type="email" /></td>

									</tr>

									<tr>
										<td><form:errors path="gender" /></td>
										<td><label>Gender</label></td>
										<td><form:select path="gender" items="${radio}" /></td>

									</tr>

									<tr>
										<td><input class="btn btn-success btn-lg" type="submit"
											value="REGISTER" /></td>
										<td><input class="btn btn-warning btn-lg" type="reset"
											value="Clear" /></td>
										<td><input class="btn btn-primary btn-lg" align="right"
											action="action" type="button" onclick="history.go(-1);"
											value="Back" /></td>
									</tr>

								</table>

							</form:form>
					</center>
				</span>
			</div>
		</div>
		</div>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
	</script>
</body>
</html>