<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<title>Delete</title>
<style>
#t {
	border-collapse: separate;
	border-spacing: 20px 30px;
}

td {
	color: #FDFEFE;
	font-weight: bold;
	font-size: 130%;
}

body {
	background-image: url('../images/deletestud.jpg');
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: 100% 100%;
}

#mydiv {
	position: fixed;
	top: 20%;
	left: 50%;
	transform: translate(-50%, -50%);
}
</style>
</head>
<body>
<jsp:include page="header.jsp" /> 
<c:if test="${not empty loggedInUser.userID}">
	<div id="mydiv">
		<div class="container-fluid">
			<!-- <span class="border border-success rounded-lg"> -->
			<center>
				<h1 class="page-header text-center" style="color: #F39C12">Delete
					Student Details</h1>

				<div class="container" align="center">

					<form:form method="post" modelAttribute="id">
						<table id="t">
							<tr>
								<td><form:label path="student_id">Enter Student ID to delete</form:label></td>
								
							</tr>
							<tr>
								<td><form:input path="student_id" required="required"/></td>
								<td>${msg}</td>
								<td><form:errors path="student_id" /></td>
							</tr>
							<tr>
								<td align="right"><input class="btn btn-success"
									type="submit" value="Delete"></td>
								<td align="left"><a class="btn btn-primary" href="/homepage"> Back</a></td>
							</tr>
						</table>
					</form:form>
				</div>
			</center>
			<!-- </span> -->
		</div>
	</div>
	</c:if>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
</body>
</html>