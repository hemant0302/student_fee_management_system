<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Delete</title>
<style type="text/css">
#t {
	border-collapse: separate;
	border-spacing: 50px 30px;
}

td {
	/* color: #FDFEFE; */
	font-weight: bold;
	font-size: 130%;
}

body {
	background-image: url('../images/admindelete.jpg');
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: 100% 100%;
}

#mydiv {
	position: fixed;
	top: 43%;
	left: 50%;
	transform: translate(-50%, -50%);
}
</style>
</head>
<body id="mydiv">
	<jsp:include page="header.jsp" />
	<c:if test="${not empty loggedInUser.userID}">
		<center>
			<h1 align="centre" style="color: #D35400">CHANGE PASSWORD</h1>
			<form:form action="">
				<table id="t">
					<tr>
						<td style="color: #FF00FF">UserID:${message}</td>
					</tr>
					<tr style="color: #90EE90">
						<td><label>Enter current Password</label></td>
						<td><input name="currPass" type="password"
							required="required" /></td>
					</tr>
					<tr style="color: #90EE90">
						<td><label>Enter new Password</label></td>
						<td><input name="newPass" type="password" required="required" /></td>
					</tr>
					<tr style="color: #90EE90">
						<td><label>Confirm new Password</label></td>
						<td><input name="confPass" type="password"
							required="required" /></td>
					</tr>
					<tr>

						<td><input class="btn btn-danger" type="submit"
							value="Change Password"></td>
						<td><a class="btn btn-primary" href="/homepage"> Back</a></td>

					</tr>

				</table>
			</form:form>
		</center>
	</c:if>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
</body>
</html>
