<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View</title>
<style type="text/css">
#t {
	border-collapse: separate;
	border-spacing: 20px 20px;
}

body {
	background-image: url('../images/delete.jpg');
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: 100% 100%;
}

#mydiv {
	position: fixed;
	top: 20%;
	left: 20%;
	transform: translate(-50%, -50%);
}
</style>
</head>
<body>
<jsp:include page="header.jsp" /> 
<c:if test="${not empty loggedInUser.userID}">
	<h1 class="page-header text-center" style="color: blue">Student View</h1>

	<div class="container" align="center">
		<form:form method="post" action="" modelAttribute="login">

			<%-- <sf:label path="add">Add Student</sf:label> --%>

			<%--  <sf:input path="viewStudent" id="viewStudent" type="radio" name="detailsStudent"/>View Student Details
        
        <sf:input path="updateStudent" id="updateStudent" type="radio" name="detailsStudent"/>Update Student Details
		
		<input type="submit" value="StudentSubmit" id="submit" name="submit">  --%>
			<table>
			    <tr>
			    	<td style="color: #D35400">${password}</td>
			    <tr>
				<tr>
					<td style="color: #FF0005">Student Id:${id}</td>
				</tr>
				<tr>
					
					<td><a class="btn btn-warning" href="/studentUpdate">Update
							Student Details</a></td>
							<td><a class="btn btn-info" href="/studentView">View
							Student Details</a></td>
					<td><a class="btn btn-success" href="/changePasswordS">Change
							Password</a></td>
					

				</tr>

			</table>

		</form:form>
	</div>
	</c:if>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
		
</body>
</html>